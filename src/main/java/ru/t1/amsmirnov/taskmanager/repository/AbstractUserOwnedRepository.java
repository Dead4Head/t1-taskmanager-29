package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Nullable
    @Override
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M removeOne(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        return removeOneById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}
