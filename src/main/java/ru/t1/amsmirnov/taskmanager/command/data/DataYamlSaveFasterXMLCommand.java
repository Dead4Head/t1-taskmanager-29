package ru.t1.amsmirnov.taskmanager.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.TransportFileException;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataYamlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data to YAML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[DATA SAVE YAML]");
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_YAML);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(yaml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (final Exception exception) {
            throw new TransportFileException(FILE_YAML, exception);
        }
    }

}
