package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.TransportFileException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[DATA LOAD JSON]");
            System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

}
